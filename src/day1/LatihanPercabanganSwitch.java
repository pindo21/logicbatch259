package day1;

import java.util.Scanner;

public class LatihanPercabanganSwitch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Silahkan pilih nomor : ");
		
		int nomor = input.nextInt();
		String nomorString = "";
		
//		switch(month) { //break supaya tidak berjalan terus, switch harus pakai break
//		case 1 :
//			monthString = "January";
//			break;
//		case 2 :
//			monthString = "February";
//			break;
//		case 3 :
//			monthString = "March";
//			break;
//		case 4 :
//			monthString = "April";
//			break;
//		case 5 :
//			monthString = "May";
//			break;
//		case 6 :
//			monthString = "June";
//			break;
//		case 7 :
//			monthString = "July";
//			break;
//		case 8 :
//			monthString = "August";
//			break;
//		case 9 :
//			monthString = "September";
//			break;
//		case 10 :
//			monthString = "October";
//			break;
//		case 11 :
//			monthString = "November";
//			break;
//		case 12 :
//			monthString = "December";
//			break;
//		default : // sama dengan else
//			System.out.println("Invalid Month");
//		}
//		System.out.println(monthString);
		switch(nomor) {
		case 1 :
			nomorString = "Tisu";
			break;
		case 2 :
			nomorString = "Kopi Kenangan";
			break;
		case 3 :
			nomorString = "Susu Kental Manis";
			break;
		case 4 :
			nomorString = "Minyak Goreng";
			break;
		case 5 :
			nomorString = "Uang Rp. 50.000";
			break;
		default :
			System.out.println("Nomer Hadiah yang Anda Pilih Salah!");	
		}
		
		if (nomorString.equals("")) {
			System.out.println("Kamu gagal mendapatkan hadiah");
		} else {
			System.out.println("Selamat kamu mendapatkan = Hadiah " + nomorString);
		}
	}

}
