package Strings;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class twoStrings10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		Set<Character> kata1 = new HashSet<>();
		Set<Character> kata2 = new HashSet<>();
		
		System.out.print("Kata pertama = ");
		String s1 = input.nextLine();
		for (char c : s1.toCharArray()) {
			kata1.add(c);
		}
		
		System.out.print("Kata kedua = ");
		String s2 = input.nextLine();
		for (char c : s2.toCharArray()) {
			kata2.add(c);
		}
		
		kata1.retainAll(kata2);
		
		if (!kata1.isEmpty()) {
			System.out.println("yes");
		} else {
			System.out.println("no");
		}
	}

}
