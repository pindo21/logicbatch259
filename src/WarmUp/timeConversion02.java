package WarmUp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class timeConversion02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		
		System.out.print("waktu yang akan  dikonversi :");
		String n = input.nextLine().toLowerCase();
		DateFormat waktuAwal = new SimpleDateFormat("hh:mm:ssaa");
		DateFormat waktuAkhir = new SimpleDateFormat("HH:mm:ss");
		
		Date waktu = null ;
		String output = null ;
		
		try {
			waktu = waktuAwal.parse(n);
			output = waktuAkhir.format(waktu);
			System.out.println("setelah dikonversi : " + output);
		} catch (ParseException x) {
			x.printStackTrace();
		}
	}

}
