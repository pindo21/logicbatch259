package WarmUp;

import java.util.Scanner;

public class diagonalDifference04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Panjang Matriks = ");
		int n = input.nextInt();

		int[][] matriks = new int[n][n];
		int jumlah1 = 0;
		int jumlah2 = 0;

		// input nilai matriks
		for (int i = 0; i < matriks.length; i++) {
			for (int j = 0; j < matriks.length; j++) {
				int a = input.nextInt();
				matriks[i][j] = a;
			}
		}

		// menampilkan matriks
		for (int i = 0; i < matriks.length; i++) {
			for (int j = 0; j < matriks.length; j++) {
				System.out.print(matriks[i][j] + " ");
			}
			System.out.println();
		}

		for (int i = 0; i < matriks.length; i++) {
			for (int j = 0; j < matriks.length; j++) {
				if (i == j) {
					jumlah1 = jumlah1 + matriks[i][j];
				}
			}
		}

		for (int i = 0; i < matriks.length; i++) {
			for (int j = 0; j < matriks.length; j++) {
				if ((i + j) == (n - 1)) {
					jumlah2 = jumlah2 + matriks[i][j];
				}
			}
		}

		System.out.println("Total = " + Math.abs(jumlah1 - jumlah2));
	}

}
