package WarmUp;

import java.util.Scanner;

public class AVeryBigSum09 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.print("Banyak array = ");
		int n = input.nextInt();
		
		long[] angka = new long[n];
		long jumlah = 0 ;
		
		for (int i = 0; i < n; i++) {
			long x = input.nextLong();
			angka[i] = x ;
		}
		
		for (int i = 0; i < n; i++) {
			jumlah = jumlah + angka[i];
		}
		System.out.print("Jumlah = " + jumlah);
	}

}
