package day2;

import java.util.Scanner;

public class LatihanNestedLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// soal 1
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan nilai n = ");
		int n = input.nextInt();
		System.out.print("Masukkan nilai n2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int x = 1;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else {
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
					x *= n2 ;
				}

			}
			System.out.println();
		}
		input.close();
	}

}
