package Pretest1;

import java.util.Arrays;
import java.util.Scanner;

public class soal02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan kalimat = ");
		
		//
		String kalimat = input.nextLine().toLowerCase().replaceAll(" " , "") ;

		char[] huruf = kalimat.toCharArray();

		String vokal = "";
		String konsonan = "";
		
		Arrays.sort(huruf);
		for (int i = 0; i < huruf.length; i++) {
			if (huruf[i] == 'a' || huruf[i] == 'i' || huruf[i] == 'u' || huruf[i] == 'e' || huruf[i] == 'o') {
				vokal = vokal + huruf[i];
			} else {
				konsonan = konsonan + huruf[i];
			}	
		}
		
		System.out.println("Huruf Vokal = " + vokal );
		System.out.println("Huruf Konsonan = " + konsonan);

	}

}
